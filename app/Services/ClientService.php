<?php

namespace App\Services;

use App\Repositories\ClientRepository;

class ClientService
{
    private $clientRepository;

    /**
     * ClientService constructor.
     * @param ClientRepository $clientRepository
     */
    public function __construct( ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * @param array $data
     * @return \App\Models\Client
     */
    public function create(array $data)
    {
        return $this->clientRepository->create($data);
    }
}

