<?php

namespace App\Services;

use App\Repositories\OrderRepository;
use App\Repositories\DriverRepository;


class OrderService
{
    private $orderRepository;
    private $driverRepository;

    /**
     * OrderService constructor.
     * @param OrderRepository $orderRepository
     * @param DriverRepository $driverRepository
     */
    public function __construct( OrderRepository $orderRepository, DriverRepository $driverRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->driverRepository = $driverRepository;
    }

    /**
     * Get all orders
     * @param array $options
     * @return mixed
     */
    public function getAll($options = [])
    {
        return $this->orderRepository->get($options);
    }

    /**
     * Create a record of Order
     * @param $client_id
     * @param array $data
     * @return \App\Models\Order
     */
    public function create($client_id, array $data)
    {
        //insertamos registro
        //genero un codigo de pedido único:
        do {
            $code = str_random(10);
        } while ($this->orderRepository->get_byCode($code) > 0);

        $data['client_id'] = $client_id;
        $data['code'] = $code;

        $order = $this->orderRepository->create($data);

        //actualizamos registro
        //generamos un Driver aleatorio, para asociar al pedido y crarlo
        $driver = $this->driverRepository->randon_record();
        $data_update['driver_id'] = $driver->id;

        $order = $this->orderRepository->update($order, $data_update);

        return $order;
    }

    /**
     * Update record of Order
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function update(array $data, $id)
    {
        $order = $this->getRequested($id);
        $order = $this->orderRepository->update($order, $data);
        return $order;
    }

    /**
     * Delete record of Order
     * @param $id
     */
    public function delete($id)
    {
        $this->orderRepository->delete($id);
    }

    /**
     * Get order with selected id
     * @param $id
     * @return mixed
     */
    public function getRequested($id)
    {
        $order = $this->orderRepository->getById($id);
        if (is_null($order)) {
            return ['error_id' => 'KO', 'error' => 'Record not found.'];
        }
        return $order;
    }

    /**
     * Get one day's orders, for one driver
     * @param $driver
     * @param $date
     * @return mixed
     */
    public function get_ordersDay($driver, $date){
        return $this->orderRepository->get_ordersDay($driver, $date);
    }

}