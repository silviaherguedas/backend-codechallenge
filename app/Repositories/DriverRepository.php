<?php

namespace app\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\Driver;
use Optimus\Genie\Repository;

class DriverRepository extends Repository
{
    /**
     * @return Order
     */
    protected function getModel()
    {
        return new Driver;
    }

    /**
     * Create a record of Driver
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function create(array $data)
    {
        DB::beginTransaction();

        try {
            $driver = Driver::firstOrCreate($data);
        } catch(Exception $e) {
            DB::rollBack();

            throw $e;
        }

        DB::commit();

        return $driver;
    }

    public function total_records(){
        return Driver::count();
    }

    /**
     * generate a randon record of Driver
     */
    public function randon_record(){
        $randomUser =DB::table('drivers')
            ->inRandomOrder()
            ->first();
        return $randomUser;
    }

}