<?php

namespace App\Repositories;

use App\Models\Order;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\OrdersDayResource;
use Optimus\Genie\Repository;

class OrderRepository extends Repository
{
    /**
     * @return Order
     */
    protected function getModel()
    {
        return new Order;
    }

    /**
     * Create a record of Order
     * @param array $data
     * @return Order
     * @throws \Exception
     */
    public function create(array $data)
    {
        DB::beginTransaction();

        try {
            $order = new Order();

            $order->fill($data);
            $order->save();
        } catch(Exception $e) {
            DB::rollBack();

            throw $e;
        }

        DB::commit();

        return $order;
    }

    /**
     * Update record of Order
     * @param Order $order
     * @param array $data
     * @return Order
     */
    public function update(Order $order, array $data)
    {
        $order->fill($data);
        $order->save();
        return $order;
    }

    /**
     * Get one day's orders, for one driver
     * @param $driver_id
     * @param $date
     * @return mixed
     */
    public function get_ordersDay($driver_id, $date){
        $results = DB::table('orders as o')
            ->select('c.name', 'c.surnames', 'c.phone',
                'o.code', 'o.address', 'o.postcode', 'o.town', 'o.city',
                'o.since', 'o.until')
            ->join('clients as c', 'c.id', '=', 'o.client_id')
            ->where('o.driver_id', '=', $driver_id)
            ->where('o.delivery_date', '=', $date)
            ->get();
        return OrdersDayResource::collection($results);
    }

    /**
     * Get total orders with selected code
     * @param $code
     * @return mixed
     */
    public function get_byCode($code)
    {
        $order = Order::where("code", "=", $code)->count();
        return $order;
    }

}