<?php

namespace App\Repositories;

use App\Models\Client;
use Illuminate\Support\Facades\DB;
use Optimus\Genie\Repository;

class ClientRepository extends Repository
{
    /**
     * @return Order
     */
    protected function getModel()
    {
        return new Client;
    }

    /**
     * Create a record of Client
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function create(array $data)
    {
        DB::beginTransaction();

        try {
            $client = Client::firstOrCreate(['email' => $data['email']], $data);
        } catch(Exception $e) {
            DB::rollBack();

            throw $e;
        }

       DB::commit();

        return $client;
    }
}

