<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 08 Dec 2018 12:54:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Order
 * 
 * @property int $id
 * @property string $code
 * @property int $client_id
 * @property int $driver_id
 * @property string $address
 * @property int $postcode
 * @property string $town
 * @property string $city
 * @property \Carbon\Carbon $delivery_date
 * @property int $since
 * @property int $until
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Driver $driver
 * @property \App\Models\Client $client
 *
 * @package App\Models
 */
class Order extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'client_id' => 'int',
		'driver_id' => 'int',
		'postcode' => 'int',
		'since' => 'int',
		'until' => 'int'
	];

	protected $dates = [
		'delivery_date'
	];

	protected $fillable = [
        'code',
		'client_id',
		'driver_id',
		'address',
		'postcode',
		'town',
		'city',
		'delivery_date',
		'since',
		'until'
	];

	public function driver()
	{
		return $this->belongsTo(\App\Models\Driver::class);
	}

	public function client()
	{
		return $this->belongsTo(\App\Models\Client::class);
	}
}
