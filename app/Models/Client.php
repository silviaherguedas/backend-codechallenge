<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 08 Dec 2018 12:54:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Client
 * 
 * @property int $id
 * @property string $name
 * @property string $surnames
 * @property string $email
 * @property string $phone
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $orders
 *
 * @package App\Models
 */
class Client extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $fillable = [
		'name',
		'surnames',
		'email',
		'phone'
	];

	public function orders()
	{
		return $this->hasMany(\App\Models\Order::class);
	}
}
