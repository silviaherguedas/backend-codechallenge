<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Requests\OrderStoreRequest;
use App\Http\Requests\OrderRequest;
use App\Services\ClientService;
use App\Services\OrderService;


class OrderController extends Controller
{
    private $clientService;
    private $orderService;
    private $errorNotFound = ['error_id' => 404, 'error' => 'Resource not found.'];

    /**
     * OrderController constructor.
     * @param ClientService $clientService
     * @param OrderService $orderService
     */
    public function __construct(ClientService $clientService, OrderService $orderService) {
        $this->clientService = $clientService;
        $this->orderService = $orderService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response($this->orderService->getAll(),200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json($this->errorNotFound, 404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param OrderStoreRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @internal param $ \OrderStoreRequest
     */
    public function store(OrderStoreRequest  $request)
    {
        $client = $this->clientService->create($request->all());
        $order = $this->orderService->create($client['id'],$request->all());

        return response()->json(['status' => 201, 'message' => 'Object created.'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = $this->orderService->getRequested($id);
        return response($order, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json($this->errorNotFound, 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param OrderRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrderRequest $request, $id)
    {
        $order = $this->orderService->update($request->all(), $id);

        return response()->json($order, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->orderService->delete($id);
        return response()->json(['status' => 204, 'message' => 'Object delete.'], 204);
    }

    /**
     * Get one day's orders, for one driver
     * @param $driver
     * @param $year
     * @param $month
     * @param $day
     * @return mixed
     */
    public function ordersDay($driver, $year, $month, $day)
    {
        $orders = $this->orderService->get_ordersDay($driver, $year.'-'.$month.'-'.$day);
        return response()->json($orders, 200);
    }
}
