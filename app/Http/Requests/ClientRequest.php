<?php

namespace App\Http\Requests;

class ClientRequest extends BaseFormRequest
{
    private $rules = [
        'name' => 'present|required|max:50',
        'surnames' => 'present|required|max:80',
        'email' => 'present|required|email',//|unique:clients,email',
        'phone' => 'present|required|phone:ES',
    ];

    private $messages = [
    ];

    private $filters = [
        'name' => 'trim|escape|capitalize',
        'surnames' => 'trim|escape|capitalize',
        'email' => 'trim|escape|lowercase',
    ];

    /**
     * @return array
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->getRules();
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return $this->getMessages();
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return $this->getFilters();
    }
}
