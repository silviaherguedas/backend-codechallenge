<?php

namespace App\Http\Requests;


class OrderStoreRequest extends BaseFormRequest
{
    private $orderRequest;
    private $clientRequest;

    /**
     * OrderStoreRequest constructor.
     * @internal param ClientRequest $clientRequest
     * @internal param OrderRequest $orderRequest
     */
    public function __construct()
    {
        $this->orderRequest = new OrderRequest;
        $this->clientRequest = new ClientRequest;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge($this->clientRequest->getRules(), $this->orderRequest->getRules());
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return array_merge($this->clientRequest->getMessages(),$this->orderRequest->getMessages());
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return array_merge($this->clientRequest->getFilters(), $this->orderRequest->getFilters());
    }

}
