<?php

namespace App\Http\Requests;

class OrderRequest extends BaseFormRequest
{
    private $rules = array(
        'address' => 'present|required|max:150',
        'postcode' => 'present|required|numeric',//|size:5',
        'town' => 'present|required|max:150',
        'city' => 'present|required|max:150',
        'delivery_date' => 'present|required|date',
        'since' => 'present|required|numeric|between:9,20',
        'until' => 'present|required|numeric|between:9,20|gte:since|after_or_equal:since'
    );

    private $messages = array(
        'since.between' => 'Horario comprendido entre 9:00 y las 20:00',
        'until.between' => 'Horario comprendido entre 9:00 y las 20:00, y superior al horario de entrada.'
    );

    private $filters = array(
        'address' => 'trim|capitalize|escape',
        'postcode' => 'trim|cast:integer',
        'town' => 'trim|capitalize|escape',
        'city' => 'trim|capitalize|escape',
        'delivery_date' => 'trim',//|format_date:d/m/Y, Y-m-d
        'since' => 'trim|cast:integer',
        'until' => 'trim|cast:integer'
    );

    /**
     * @return array
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->getRules();
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return $this->getMessages();
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return $this->getFilters();
    }
}
