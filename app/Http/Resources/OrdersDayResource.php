<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrdersDayResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return array(
            'name' => $this->name,
            'surnames' => $this->surnames,
            'phone' => $this->phone,
            'code' => $this->code,
            'address' => $this->address,
            'postcode' => $this->postcode,
            'town' => $this->town,
            'city' => $this->city,
            'since' => $this->since,
            'until' => $this->until
        );
    }
}