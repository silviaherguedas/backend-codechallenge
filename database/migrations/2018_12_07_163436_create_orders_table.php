<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 10)->unique();
            $table->unsignedInteger('client_id')->index(); 
            $table->foreign('client_id')
                    ->references('id')->on('clients')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->unsignedInteger('driver_id')->nullable()->index();
            $table->foreign('driver_id')
                    ->references('id')->on('drivers')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');
            $table->string('address', 150);
            $table->unsignedMediumInteger('postcode');
            $table->string('town', 150);
            $table->string('city', 150);
            $table->date('delivery_date');
            //solo tomará valores para la franja horaria de 9 a 20 horas
            $table->unsignedTinyInteger('since');
            $table->unsignedTinyInteger('until');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
