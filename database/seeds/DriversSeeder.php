<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Driver;

class DriversSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('drivers')->truncate();
        factory(App\Models\Driver::class, 50)->create();
    }
}
