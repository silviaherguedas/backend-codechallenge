<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Order;

class OrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('orders')->truncate();
        factory(App\Models\Order::class, 100)->create();
    }
}
