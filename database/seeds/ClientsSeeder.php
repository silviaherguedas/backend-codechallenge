<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Client;

class ClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('clients')->truncate();
        factory(App\Models\Client::class, 150)->create();
    }
}
