<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->cleanDatabase();
        $this->truncateTables([
            'drivers',
            'clients',
            'orders',
        ]);

        Model::unguard();
        $this->call('DriversSeeder');
        $this->call('ClientsSeeder');
        $this->call('OrdersSeeder');
        Model::reguard();
    }

    /**
     * Delete the application's database.
     *
     * @return void
     */
    public function truncateTables(array $tables)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
 
        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }
 
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }
}
