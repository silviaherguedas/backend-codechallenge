<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Order::class, function (Faker $faker) {
    $hora_ini = 9;// hora inicio entregas
    $hora_fin = 20;// hora fin entregas
    $hora_max = 8;// máximo franjas horarias 8h
    
    //control de la franja horaria inicial
    $since = $faker->numberBetween($hora_ini, $hora_fin);
    if($since == $hora_fin) $since--; // si el desde coincide con la hora_fin, restamos un valor
    
    //control de la franja horaria final
    $until = $faker->numberBetween($since, $hora_fin);
    if($until == $since and $until < $hora_fin) $until++;
    else if(($until - $since) > $hora_max) $until = $since + $hora_max;
    
    //generar faker
    return [
        'code' => $faker->bothify('#?#?##??##'),
        'client_id' => $faker->numberBetween(1, 150),
        'driver_id' => $faker->numberBetween(1, 50),
        'address' => $faker->streetAddress,
        'postcode' => $faker->postcode,
        'town' => $faker->city,
        'city' => $faker->state,
        'delivery_date' => $faker->dateTimeBetween('+ 1 days', '+ 5 days')->format('Y-m-d'),
        'since' => $since,
        'until' => $until,
    ];
});
