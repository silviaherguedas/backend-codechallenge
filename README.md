# Code Challenge - Backend

Hola Eduardo,

También quería agradeceros a vosotros la oportunidad de poder demostrar mi forma de trabajar con esta práctica.

Comento algunas cosas para la correcta instalación y sobre el trabajo realizado en esta práctica.

Versión del LAMP utilizado:

- Apache: 2.4.33
- MySQL: 5.7.21
- PHP: 7.2.4

# Modelo de Datos (SQL)

Toda la lógica del modelo de datos se encuentra desarrollada en los archivos de migraciones de Laravel. Del mismo modo, las tablas pueden ser pobladas ejecutando el comando seed.  Pues he creado el modelo de fábrica apropiado a cada tipo de dato.

He dejado en la carpeta \storage\sql\, un script con el que poder crear una base de datos y un usuario con permisos sobre dicha base de datos, para que pueda ser utilizado en la práctica. El documento es el siguiente:

.\storage\sql\goiDb.sql

El srcipt SQL está exportado desde PHPMyAdmin (v 4.7.9), borra la base de datos entera y la vuelve a crear (siempre que se tengan permisos root). Por lo que se puede ejecutar tantas veces como sea necesario.

Los datos de conexión de la herramienta web desde PHP son los siguientes:

- DB_CONNECTION=mysql
- DB_HOST=127.0.0.1
- DB_PORT=3306
- DB_DATABASE=goidb
- DB_USERNAME=app.goi
- DB_PASSWORD=qD1zu8eYF7OG30Gx

# Desarrollo de la herramienta

Versión de Laravel: 5.7

He adaptado la estructura que nos proporciona Laravel, al patrón “service-repository” (patrón de capas).

El cual crea una capa de abstracción entre la capa de acceso a datos y la capa de lógica de negocios de la aplicación. Considero que este patrón es el más adecuado en una API REST, ya que puede ayudar a aislar la aplicación de cualquier cambio entre la base de datos (repository) y las operaciones de lógica de negocio (service). Además de facilitar la realización de pruebas unitarias automatizadas o el desarrollo controlado por pruebas (TDD).

La implementación tiene un único propósito. No tener lógica de negocio en nuestros repositorios, sólo operaciones sobre la capa de persistencia.

La estructura de carpetas ha quedado de la siguiente forma:

### Controller:
Es el punto de entrada y salida para la aplicación. Realizará la validación simple utilizando la validación de solicitud de Laravel (Request) y define los puntos finales de nuestros recursos. El controlador llamará al método apropiado del Servicio y formateará la respuesta en JSON con el código de estado correcto.

### Services Class: 
Las clases de este tipo, serán las encargadas contener la lógica empresarial, funcionarán como punto de unión que une diferentes clases para llevar a cabo una operación.
Son el paso intermedio entre el controlador y el repositorio.

### Repositories: 
El patrón de repositorio es la capa para la interacción con los modelos y para realizar las operaciones contra la base de datos. 
De esta manera separamos la lógica de negocios y la lógica de SQL de nuestra aplicación. 

## Paquetes externos

También he utilizado como capas las clases Request y Resources que aporta Laravel, para la validación de variables que llegan a la API a través del controlador y para transformar las consultas que devuelve el modelo en sencillas respuestas JSON.

Adicionalmente y para complementar algunas de las acciones he instalado las siguientes dependencias:
- Reliese Laravel: https://github.com/reliese/laravel 

Colección de componentes cuyo objetivo es ayudar en el proceso de desarrollo de una aplicación en Laravel, para la generación de código outomático. 
En mi caso lo he utilizado para la creación de los modelos de la base de datos.

<div class="highlight highlight-source-shell"><pre>php artisan vendor:publish --tag=reliese-models</pre></div>

<div class="highlight highlight-source-shell"><pre>php artisan config:clear</pre></div>

- WAAVI Sanitizer: https://github.com/Waavi/Sanitizer 

Proporciona una forma fácil de formatear las entradas del usuario, mediante filtros tanto predefinidos como personalizados.

En mi caso, lo he utilizado para estandarizar las variables que llegan a la API para ser insertadas contra la base de datos.
- Laravel Phone: https://github.com/Propaganistas/Laravel-Phone
 
Agrega la funcionalidad de validación de números de teléfono según el país.
- Laravel Español: https://github.com/Laraveles/spanish

Paquete de traducciones en español para Laravel 5.

- Genie: https://github.com/esbenp/genie

Clase base de la que extenderá la capa Repository. Con métodos que cubren la mayoría de las consultas. Útil para abstraer su capa de persistencia de su código de negocio.

# Definición de los EndPoints

He utilizado Postman, para las pruebas contra la API REST, os comparto el enlace de la colección que he creado en mi cuenta, para que tengáis acceso a las rutas configuradas.

https://www.getpostman.com/collections/4ff04a5cbe7b67512d73 

### Endpoint para persistir el pedido en BD:
- URL: http://127.0.0.1:8000/api/orders
- Cabeceras:
 
Content-Type => application/json

X-Requested-With => XMLHttpRequest

- JSON que recibe la API (Post):

<div class="highlight highlight-source-json"><pre>
{
    "name": "Silvia M",
    "surnames": "HERGEDAS ESTEBAN",
    "email": "she2568@example.net",
    "phone": "983000000",
    "address":"dirección entrega",
    "postcode":47014,
    "town": "localidad de entrega",
    "city": "Valladolid",
    "delivery_date": "2018-12-28",
    "since": 10,
    "until": 16
}
</pre></div>

### Endpoint para mostrar los pedidos a entregar por los drivers

-	URL: http://127.0.0.1:8000/api/ordersDay/22/2018/12/12 

http://127.0.0.1:8000/api/ordersDay/{driver}/{year}/{month}/{day}	=> rellenar según los valores de base de datos.

-	Cabeceras:
 
Content-Type => application/json

-	JSON que devuelve la API (Get):

<div class="highlight highlight-source-json"><pre>
[
    {
        "name": "Samuel",
        "surnames": "Perea Lorente",
        "phone": "689-674876",
        "code": "7u8x01lu79",
        "address": "Camiño Alemán, 151, Bajo 5º",
        "postcode": 67801,
        "town": "Berríos Alta",
        "city": "Burgos",
        "since": 13,
        "until": 18
    },
    {
        "name": "Gonzalo",
        "surnames": "Sancho Sánchez",
        "phone": "998-398641",
        "code": "8i5r02zk43",
        "address": "Rúa Claudia, 8, 91º 6º",
        "postcode": 3625,
        "town": "Los Gallardo",
        "city": "Zaragoza",
        "since": 19,
        "until": 20
    },
    {
        "name": "Pol",
        "surnames": "Delafuente Romo",
        "phone": "937 984923",
        "code": "5p7x54ff75",
        "address": "Praza Hernándes, 6, Ático 3º",
        "postcode": 54990,
        "town": "Las Covarrubias de Ulla",
        "city": "Illes Balears",
        "since": 11,
        "until": 16
    }
]
</pre></div>

Estoy a vuestra disposición para resolver cualquier duda.

Muchas gracias

Silvia
