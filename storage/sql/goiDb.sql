-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 12-12-2018 a las 18:17:44
-- Versión del servidor: 5.6.10-log
-- Versión de PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Base de datos: `goidb`
--
DROP DATABASE IF EXISTS `goidb`;
CREATE DATABASE IF NOT EXISTS `goidb` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `goidb`;

-- --------------------------------------------------------

--
-- Usuario asociado a la base de datos
--

DROP USER "app.goi"@"localhost";
CREATE USER 'app.goi'@'localhost' IDENTIFIED WITH mysql_native_password;
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, FILE, REFERENCES, INDEX, ALTER, LOCK TABLES ON *.* TO 'app.goi'@'localhost' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
SET PASSWORD FOR 'app.goi'@'localhost' = PASSWORD('qD1zu8eYF7OG30Gx');
GRANT ALL PRIVILEGES ON `goidb`.* TO 'app.goi'@'localhost' WITH GRANT OPTION;
