<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::get('/ordersDay/{driver}/{year}/{month}/{day}', 'OrderController@ordersDay')
        ->where(['driver' => '[0-9]+', 'year' => '[0-9]{4}', 'month' => '[0-9]{1,2}', 'day' => '[0-9]{1,2}'])
        ->name('ordersDay');

//Agregamos nuestra ruta al controller de Order
Route::resource('orders', 'OrderController');

//Agregamos nuestra ruta al controller de Client
Route::resource('clients', 'ClientController');